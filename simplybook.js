const Auth = require('./service/auth.service')
const Admin = require('./service/admin.service')
const Public = require('./service/public.service')
const Catalogue = require('./service/catalogue.service')

const BookFilter = require('./model/book.model')
const ServiceCategory = require('./model/category.model')
const Client = require('./model/client.model')
const {ServiceProvider, ServiceProviderLocation} = require('./model/provider.model')
const {Service, WorkDayInfo}  = require('./model/service.model')

module.exports = class SimplyBook {

    constructor(companyName, apiKey, secretKey, account, password) {
        this.config = {
            CompanyName: companyName,
            ApiKey: apiKey,
            SecretKey: secretKey,
            Account: account,
            Password: password
        }
    }

    // ---- setter函數
    get Config() {
        return this.config
    }

    /**
     * Authentication
     */
    createAuthService() {
        return new Auth(this.config)
    }

    /**
     * Company administration service
     */
    createAdminService(token) {
        return new Admin(this.config, token)
    }

    /**
     * Company public service
     */
    createPublicService(token) {
        return new Public(this.config, token)
    }

    /**
     * Catalogue
     */
    createCatalogueService(token) {
        return new Catalogue(this.config, token)
    }

    // ===========================================================
    /**
     * 訂單Filter Model
     */
    createBookFilterModel(startDate) {
        return new BookFilter(startDate)
    }

    /**
     * 服務類別相關 Model
     */
    createCategoryModel(categoryName, description) {
        return new ServiceCategory(categoryName, description)
    }

    /**
     * 顧客相關 Model
     */
    createClientModel(userName, userPhone, userEmail) {
        return new Client(userName, userPhone, userEmail)
    }

    /**
     * 服務提供者地區 Model
     */
    createProviderLocationModel(locationTitle, description) {
        return new ServiceProviderLocation(locationTitle, description)
    }

    /**
     * 服務提供者 Model
     */
    createProviderModel(providerName, description) {
        return new ServiceProvider(providerName, description)
    }

    /**
     * 服務相關 Model
     */
    createServiceModel(serviceName, description) {
        return new Service(serviceName, description)
    }

    /**
     * 設定work day Model
     */
    createWorkDayInfoModel(startTime, endTime) {
        return new WorkDayInfo(startTime, endTime)
    }
}
