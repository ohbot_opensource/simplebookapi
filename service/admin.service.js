const BaseService = require('./base.service')
const http = require('../lib/https')

module.exports = class AdminService extends BaseService{

    /**
     * 建構子
     * @param config
     * @param token
     */
    constructor(config, token){

        let option = {
            'X-Company-Login': config.CompanyName,
            'X-User-Token': token,
            'Content-Type': 'application/json',
        }

        super(config.CompanyName, '/admin', option)
    }
    // ================================
    //           General API
    // ================================
    /**
     * 取得國家列表
     */
    getCountryList() {
        let postData = []

        return http.post(this.RequestOption,
            this.BaseURL,
            "getCountryList",
            postData)
    }

    /**
     * 取得國際電話編碼
     */
    getCountryPhoneCodes() {
        let postData = []

        return http.post(this.RequestOption,
            this.BaseURL,
            "getCountryPhoneCodes",
            postData)
    }

    /**
     *  可以設定休息時間、工作時間、是否為休假日、特殊日
     *  可以同時設定多組參數
     */
    setWorkDayInfo(...workDay) {
        let postData = workDay

        return http.post(this.RequestOption,
            this.BaseURL,
            "setWorkDayInfo",
            postData)
    }

    // ================================
    //           Service API
    // ================================
    /**
     * 取得服務列表
     */
    getEventList() {
        let postData = []

        return http.post(this.RequestOption,
            this.BaseURL,
            "getEventList",
            postData)
    }

    /**
     * 新增服務
     */
    addService(serviceModel) {
        let postData = this.setArray(serviceModel)

        return http.post(this.RequestOption,
            this.BaseURL,
            "addService",
            postData)
    }

    /**
     * 修改服務
     */
    editService(serviceId, serviceModel) {
        let postData = this.setArray(serviceId, serviceModel)

        return http.post(this.RequestOption,
            this.BaseURL,
            "editService",
            postData)
    }

    // ================================
    //           Provider API
    // ================================
    /**
     * 取得服務列表
     */
    getUnitList() {
        let postData = []

        return http.post(this.RequestOption,
            this.BaseURL,
            "getUnitList",
            postData)
    }

    /**
     * 新增服務人員
     */
    addServiceProvider(providerModel) {
        let postData = this.setArray(providerModel)

        return http.post(this.RequestOption,
            this.BaseURL,
            "addServiceProvider",
            postData)
    }

    /**
     * 編輯服務人員
     */
    editServiceProvider(providerId, providerModel) {
        let postData = this.setArray(providerId, providerModel)

        return http.post(this.RequestOption,
            this.BaseURL,
            "editServiceProvider",
            postData)
    }

    // ================================
    //           Category API
    // ================================
    /**
     * 取得服務類別列表
     */
    getCategoriesList(isPublic) {
        let postData = this.setArray(isPublic)

        return http.post(this.RequestOption,
            this.BaseURL,
            "getCategoriesList",
            postData)
    }

    /**
     * 新增服務類別
     */
    addServiceCategory(categoryModel) {
        let postData = this.setArray(categoryModel)

        return http.post(this.RequestOption,
            this.BaseURL,
            "addServiceCategory",
            postData)
    }

    /**
     * 編輯服務類別
     */
    editServiceCategory(categoryId, categoryModel) {
        let postData = this.setArray(categoryId, categoryModel)

        return http.post(this.RequestOption,
            this.BaseURL,
            "editServiceCategory",
            postData)
    }

    /**
     * 刪除服務類別
     */
    deleteServiceCategory(categoryId) {
        let postData = this.setArray(categoryId)

        return http.post(this.RequestOption,
            this.BaseURL,
            "deleteServiceCategory",
            postData)
    }
    // ================================
    //           Client API
    // ================================
    getClientInfo() {
        let postData = []

        return http.post(this.RequestOption,
            this.BaseURL,
            "getClientInfo",
            postData)
    }

    addClient(clientData, isSendEmail) {
        let postData = this.setArray(clientData, isSendEmail)

        return http.post(this.RequestOption,
            this.BaseURL,
            "addClient",
            postData)
    }

    editClient(clientId, clientData) {
        let postData = this.setArray(clientId, clientData)

        return http.post(this.RequestOption,
            this.BaseURL,
            "editClient",
            postData)
    }

    changeClientPassword(clientId, password, isSendEmail) {
        let postData = this.setArray(clientId, password, isSendEmail)

        return http.post(this.RequestOption,
            this.BaseURL,
            "changeClientPassword",
            postData)
    }

    // ================================
    //           Book API
    // ================================
    getBookings(filterData) {
        let postData = []
        return http.post(this.RequestOption,
            this.BaseURL,
            "getBookings",
            postData)
    }

    getBookingDetails(bookingId) {
        let postData = this.setArray(bookingId)
        return http.post(this.RequestOption,
            this.BaseURL,
            "getBookingDetails",
            postData)
    }

    editBooking(bookId, eventId, unitId, clientId, startDate, startTime, endDate, endTime){
        let postData = this.setArray(bookId, eventId, unitId, clientId, startDate, startTime, endDate, endTime)

        return http.post(this.RequestOption,
            this.BaseURL,
            "editBook",
            postData)
    }

    cancelBooking(bookingId) {
        let postData = this.setArray(bookingId)

        return http.post(this.RequestOption,
            this.BaseURL,
            "cancelBooking",
            postData)
    }
}
