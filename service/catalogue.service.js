const BaseService = require('./base.service')
const http = require('../lib/https')

module.exports = class CatalogueService extends BaseService{

    /**
     * 建構子
     * @param config
     * @param token
     */
    constructor(config, token){

        let option = {
            'X-Company-Login': config.CompanyName,
            'X-User-Token': token,
            'Content-Type': 'application/json',
        }

        super(config.CompanyName, '/catalog', option)
    }
}
