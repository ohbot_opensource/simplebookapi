
module.exports = class BaseService {

    /**
     * 建構子
     * @param companyName
     * @param url
     * @param option
     */
    constructor(companyName, url, option = {'Content-Type': 'application/json'}) {

        /**
         * 基本的URL根據不同的Service提供不同URL
         */
        this.BaseURL = 'https://user-api.simplybook.asia' + url

        /**
         * 基本的RequestOption根據不同的Service提供不同RequestOption
         */
        this.RequestOption = option

        /**
         * 紀錄公司名稱
         */
        this.CompanyName = companyName
    }

    /**
     * 將參數變成Array
     * @param data
     * @returns {*[]}
     */
    setArray(...data) {
        return data
    }
};
