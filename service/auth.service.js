const BaseService = require('./base.service')
const http = require('../lib/https')

module.exports = class AuthService extends BaseService{

    /**
     * 建構子
     * @param config
     */
    constructor(config){
        super(config.CompanyName, '/login')

        /**
         * 公司名稱
         */
        this.CompanyName = config.CompanyName

        /**
         * SimplyBook 提供的 API Key
         */
        this.ApiKey = config.ApiKey

        /**
         * SimplyBook 的 Admin 帳號
         */
        this.Account = config.Account

        /**
         * SimplyBook 的 Admin 密碼
         */
        this.Password = config.Password
    }

    /**
     *
     * @returns {Promise<string>}
     */
    getToken() {
        let postData = [
            this.CompanyName,
            this.ApiKey
        ]

        return http.post(this.RequestOption,
            this.BaseURL,
            "getToken",
            postData)
    }

    /**
     *
     * @returns {Promise<string>}
     */
    getUserToken() {
        let postData = [
            this.CompanyName,
            this.Account,
            this.Password
        ]

        return http.post(this.RequestOption,
            this.BaseURL,
            "getUserToken",
            postData)
    }
}
