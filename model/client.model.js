module.exports = class Client {

    constructor(userName, phone, email){
        /**
         * 顧客姓名
         */
        this.name = userName

        /**
         * 顧客電子信箱
         */
        this.phone = phone

        /**
         * 顧客電子信箱
         */
        this.email = email

        /**
         * 顧客地址1
         */
        this.address1 = ''

        /**
         * 顧客地址2
         */
        this.address2 = ''

        /**
         * 顧客的城市
         */
        this.city = ''

        /**
         * 顧客的郵遞區號
         */
        this.zip = ''
    }
}
