
module.exports = class ServiceCategory {

    constructor(categoryName, description){
        /**
         * 類別名稱
         */
        this.name = categoryName

        /**
         * 類別敘述
         */
        this.description = description

        /**
         * 是否為預設類別
         */
        this.is_default = '0'

        /**
         * 類別排序
         */
        this.position = '0'
    }
}
